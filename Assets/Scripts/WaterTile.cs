﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaterTile : MonoBehaviour
{
    public object _shipClass;
    public int _ship;
    private GameManager _gameManagerScript;

    [SerializeField]
    private Sprite _spriteMissed;
    [SerializeField]
    private Sprite[] _spriteHit;
    private int _spriteHitIndex;
    private SpriteRenderer _spriteRenderer;

    private SoundManager _soundManagerScript;

    /// <summary>
    /// Calls the WaterOnHit() function for each tile that was hit
    /// </summary>
    private void OnMouseDown()
    {
        if(_gameManagerScript._weapons[_gameManagerScript._activeWeapon]._currentAmmunition <= 0 || _gameManagerScript.IsOutOfAmmo() || _spriteRenderer.sprite != null)
        {
            return;
        }

        PlaySound();

        if (_gameManagerScript._activeWeapon == "Single shot")
        {
            _gameManagerScript._weapons[_gameManagerScript._activeWeapon].Shoot();
            WaterOnHit(this.gameObject);
        }
        else
        {
            RaycastHit2D[] hits = _gameManagerScript._weapons[_gameManagerScript._activeWeapon].Shoot(this.gameObject);

            foreach (RaycastHit2D hit in hits)
            {
                hit.transform.gameObject.GetComponent<WaterTile>().WaterOnHit(hit.transform.gameObject);
            }
        }

    }

    /// <summary>
    /// Checks if this tile contains a ship
    /// </summary>
    /// <param name="tile"></param>
    public void WaterOnHit(GameObject tile)
    {
        int points;
        _spriteHitIndex = Random.Range(0, _spriteHit.Length);
        _gameManagerScript.UpdateShotsFired();


        if (this.gameObject.tag == "Ship")
        {
            points = (int)_shipClass.GetType().GetMethod("PointsOnHit").Invoke(_shipClass, new object[] { _ship });
            _gameManagerScript.UpdateScore(points);
            ChangeSprite(_spriteHit[_spriteHitIndex]);
            //_soundManagerScript.PlayHitSound();
        }
        else
        {
            ChangeSprite(_spriteMissed);
            //_soundManagerScript.PlayMissedSound();
        }
    }

    /// <summary>
    /// Changes the current sprite
    /// </summary>
    private void ChangeSprite(Sprite sprite)
    {
        _spriteRenderer.sprite = sprite;
    }

    /// <summary>
    /// Plays the missed or hit sound wether the single or area shot is used or the shot is missed
    /// </summary>
    private void PlaySound()
    {
        if ((this.gameObject.tag == "Ship" && _gameManagerScript._activeWeapon == "Single shot") || _gameManagerScript._activeWeapon == "Area shot")
        {
            _soundManagerScript.PlayHitSound();
        }
        else if (this.gameObject.tag != "Ship" && _gameManagerScript._activeWeapon == "Single shot")
        {
            _soundManagerScript.PlayMissedSound();
        }
    }

    /// <summary>
    /// Called first
    /// </summary>
    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        _soundManagerScript = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }
}
