﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    public Text _scoreText;
    public Text _shotsLeftText;
    public Text _activeWeaponText;
    public Text _tutorialText;


    public Image _activeWeaponSprite;
    public Sprite _singleShotSprite;
    public Sprite _areaShotSprite;

    /// <summary>
    /// Starts another game
    /// </summary>
    public void RestartGameOnClick()
    {
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// Quits the game
    /// </summary>
    public void QuitGameOnClick()
    {
        Application.Quit();
    }

    /// <summary>
    /// Updates the score label
    /// </summary>
    /// <param name="points"></param>
    public void UpdateScoreText(int score)
    {
        _scoreText.text = "SCORE " + score;
    }

    /// <summary>
    /// Updates the remaining shots text
    /// </summary>
    /// <param name="shotsLeft"></param>
    public void UpdateShotsLeftText(int shotsLeft)
    {
        _shotsLeftText.text = "Only " + shotsLeft + " shots left!";
    }

    /// <summary>
    /// Updates the label for the currently active weapon
    /// </summary>
    /// <param name="activeWeapon"></param>
    public void UpdateActiveWeaponSprite (string activeWeapon)
    {
        if (activeWeapon == "Single shot")
        {
            //_activeWeaponSprite.color = Color.red;
            _activeWeaponSprite.sprite = _singleShotSprite;
        }
        else
        {
            //_activeWeaponSprite.color = Color.blue;
            _activeWeaponSprite.sprite = _areaShotSprite;
        }
    }

    public void UpdateTutorialText(int key)
    {
        _tutorialText.text = "Press " + key + " to change weapon";
    }
}
