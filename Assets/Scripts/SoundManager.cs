﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _hitSound;
    [SerializeField]
    private AudioClip _missedSound;

    /// <summary>
    /// Called first (even before Start())
    /// Gets the audiosource object
    /// </summary>
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Called whenever a ship was hit
    /// </summary>
    public void PlayHitSound()
    {
        _audioSource.clip = _hitSound;
        _audioSource.Play();
    }

    /// <summary>
    /// Called whenever the player missed
    /// </summary>
    public void PlayMissedSound()
    {
        _audioSource.clip = _missedSound;
        _audioSource.Play();
    }
}
