﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : Ships
{
    /// <summary>
    /// Default constructor
    /// Sets variables
    /// </summary>
    private void Awake()
    {
        _quantity = 3;
        _length = 3;
        _pointsOnHit = 200;
        _pointsOnDestroy = 300;
    }
}
