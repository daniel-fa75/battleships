﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battleship : Ships
{
    /// <summary>
    /// Default constructor
    /// Sets variables
    /// </summary>
    private void Awake()
    {
        _quantity = 1;
        _length = 5;
        _pointsOnHit = 100;
        _pointsOnDestroy = 400;
    }
}
