﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ships : ScriptableObject
{
    protected int _quantity;
    protected int _length;
    protected int _pointsOnHit;
    protected int _pointsOnDestroy;
    protected int[] _hitsTillDestroyed;

    /// <summary>
    /// OnEnable is called after Awake()
    /// Fills _hitsTillDestroyed with ships and sets the value to their length
    /// </summary>
    private void OnEnable()
    {
        _hitsTillDestroyed = new int[_quantity];

        for (int ship = 0; ship < _quantity; ship++)
        {
            _hitsTillDestroyed[ship] = _length;
        }
    }

    /// <summary>
    /// Returns the quantity of the ship
    /// _quantity is set in the ships Awake() function
    /// </summary>
    /// <returns></returns>
    public int getQuantity()
    {
        return _quantity;
    }

    /// <summary>
    /// Returns the ship's length
    /// _length is set in the ships Awake() function
    /// </summary>
    /// <returns></returns>
    public int getLength()
    {
        return _length;
    }

    /// <summary>
    /// Returns the points per hit
    /// </summary>
    /// <param name="ship"></param>
    /// <returns></returns>
    public int PointsOnHit(int ship)
    {
        return IsShipDestroyed(ship) ? _pointsOnHit + _pointsOnDestroy : _pointsOnHit;
    }

    /// <summary>
    /// Determines whether a ship is completely destroyed
    /// </summary>
    /// <param name="ship"></param>
    /// <returns></returns>
    private bool IsShipDestroyed(int ship)
    {
        _hitsTillDestroyed[ship]--;
        if (_hitsTillDestroyed[ship] <= 0)
        {
            return true;
        }
        return false;
    }
}
