﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Submarine : Ships
{
    /// <summary>
    /// Default constructor
    /// Sets variables
    /// </summary>
    private void Awake()
    {
        _quantity = 4;
        _length = 2;
        _pointsOnHit = 400;
        _pointsOnDestroy = 100;
    }
}
