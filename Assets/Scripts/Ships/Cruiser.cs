﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cruiser : Ships
{
    /// <summary>
    /// Default constructor
    /// Sets variables
    /// </summary>
    private void Awake()
    {
        _quantity = 2;
        _length = 4;
        _pointsOnHit = 300;
        _pointsOnDestroy = 200;
    }
}
