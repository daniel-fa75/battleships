﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    public float _columns = 10f;
    public float _rows = 10f;
    public string[] _shipTypes;
    public GameObject _waterTile;

    private Transform _boardHolder;
    private Transform _shipHolder;

    /// <summary>
    /// Setup of the board
    /// instance.transform.SetParent sets the gameobject as a child of the container. This is merely to keep track of the gameobjects during editing
    /// </summary>
    private void BoardSetup()
    {
        _boardHolder = GameObject.Find("Board").transform;

        for (int i = 0; i < _columns; i++)
        {
            for (int j = 0; j < _rows; j++)
            {
                GameObject instance = Instantiate(_waterTile, new Vector3(i, j, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(_boardHolder);
            }
        }
    }

    /// <summary>
    /// Placement of ships
    /// Information about ships (quantity, length) is retrieved from classes with the same name as the currently iterated shipType
    /// alignment == 0: the ship will be placed horizontally otherwise vertically
    /// Physics2D.OverlapAreaAll returns all colliders that fall within the specified area
    /// Sometimes the last ship cannot be placed because there is not enough space. Whenever that happens the scene will be loaded new
    /// </summary>
    private void SetupShips()
    {
        int length;
        int quantity;
        int alignment = 0;
        int xAxis = 0;
        int yAxis = 0;
        int failSafe = 0;

        bool placeShip = false;

        object classReference;

        Vector2 shipSize;
        Vector2 shipColliderAreaTopRight;
        Vector2 shipColliderAreaBottomLeft;
        Collider2D[] shipCollisions;
        Transform shipContainer;
        WaterTile waterTile;

        foreach (string shipType in _shipTypes)
        {
            _shipHolder = new GameObject(shipType).transform;

            classReference = ScriptableObject.CreateInstance(shipType);
            quantity = (int)classReference.GetType().GetMethod("getQuantity").Invoke(classReference, new object[] { });
            length = (int)classReference.GetType().GetMethod("getLength").Invoke(classReference, new object[] { });

            for (int ship = 0; ship < quantity; ship++)
            {
                shipContainer = new GameObject(shipType + ship).transform;
                shipContainer.SetParent(_shipHolder);

                failSafe = 0;

                do
                {

                    placeShip = true;
                    alignment = Random.Range(0, 2);

                    if (alignment == 0)
                    {
                        xAxis = (int)Random.Range(0, _columns - length);
                        yAxis = (int)Random.Range(0, _rows);
                        shipSize = new Vector2(xAxis + length - 1, yAxis);
                        shipColliderAreaTopRight = new Vector2(xAxis + length, yAxis + 1);
                    }
                    else
                    {
                        xAxis = (int)Random.Range(0, _columns);
                        yAxis = (int)Random.Range(0, _rows - length);
                        shipSize = new Vector2(xAxis, yAxis + length - 1);
                        shipColliderAreaTopRight = new Vector2(xAxis + 1, yAxis + length);
                    }

                    shipColliderAreaBottomLeft = new Vector2(xAxis - 1, yAxis - 1);

                    shipCollisions = Physics2D.OverlapAreaAll(shipColliderAreaBottomLeft, shipColliderAreaTopRight);

                    for (int result = 0; result < shipCollisions.Length; result++)
                    {
                        if (shipCollisions[result].gameObject.tag == "Ship")
                        {
                            placeShip = false;
                            break;
                        }
                    }

                    failSafe++;

                } while (!placeShip && failSafe != 1000000);

                shipCollisions = Physics2D.OverlapAreaAll(new Vector2(xAxis, yAxis), shipSize);

                if (failSafe != 1000000)
                {
                    for (int result = 0; result < shipCollisions.Length; result++)
                    {
                        shipCollisions[result].transform.SetParent(shipContainer);

                        shipCollisions[result].gameObject.tag = "Ship";
                        waterTile = shipCollisions[result].GetComponent<WaterTile>();
                        waterTile._shipClass = classReference;
                        waterTile._ship = ship;
                    }
                }
                else
                {
                    break;
                }
            }

            if(failSafe == 1000000)
            {
                SceneManager.LoadScene(1);
            }
        }
    }

    /// <summary>
    /// Calls all necessary functions to setup the board and ships
    /// </summary>
    public void SetupScene()
    {
        BoardSetup();
        SetupShips();
    }
}
