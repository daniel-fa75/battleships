﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int _score = 0;
    [HideInInspector]
    public int _shotsLeft = 0;
    [HideInInspector]
    public string _activeWeapon = "Single shot";
    public Dictionary<string, Weapon> _weapons = new Dictionary<string, Weapon>();
    private GUIController _guiControllerScript;
    private BoardManager _boardScript;

    [SerializeField]
    private GameObject _gameOverObject;

    /// <summary>
    /// Called first
    /// </summary>
	void Start ()
    {
        _guiControllerScript = GameObject.Find("Canvas").GetComponent<GUIController>();
        _boardScript = GetComponent<BoardManager>();
        InitGame();
	}

    /// <summary>
    /// Called every frame
    /// Checks for user input
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _activeWeapon = "Single shot";
            UpdateShotsFired();
            _guiControllerScript.UpdateTutorialText(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _activeWeapon = "Area shot";
            UpdateShotsFired();
            _guiControllerScript.UpdateTutorialText(1);
        }
    }

    /// <summary>
    /// Initializes the game
    /// </summary>
    private void InitGame()
    {
        InitWeapons();
        _shotsLeft = _weapons[_activeWeapon]._currentAmmunition;
        _guiControllerScript.UpdateScoreText(_score);
        _guiControllerScript.UpdateShotsLeftText(_shotsLeft);
        _guiControllerScript.UpdateTutorialText(2);
        _boardScript.SetupScene();
    }

    /// <summary>
    /// Fills the _weapons dictionary with weapon objects
    /// </summary>
    private void InitWeapons()
    {
        _weapons.Add("Single shot", new Weapon(40));
        _weapons.Add("Area shot", new Weapon(1, 2));
    }

    /// <summary>
    /// Updates the player's score
    /// </summary>
    public void UpdateScore(int points)
    {
        _score += points;
        _guiControllerScript.UpdateScoreText(_score);
    }

    /// <summary>
    /// Checks if the player is out of ammunition for every weapon
    /// </summary>
    /// <returns></returns>
    public bool IsOutOfAmmo()
    {
        foreach(KeyValuePair<string, Weapon> weapon in _weapons)
        {
            if (weapon.Value._currentAmmunition > 0)
                return false;
        }
        return true;
    }

    /// <summary>
    /// Updates the remaining ammunition
    /// Updates the label for current ammunition
    /// Checks if the game is over
    /// </summary>
    public void UpdateShotsFired()
    {
        _shotsLeft = _weapons[_activeWeapon]._currentAmmunition;
        _guiControllerScript.UpdateShotsLeftText(_shotsLeft);
        _guiControllerScript.UpdateActiveWeaponSprite(_activeWeapon);

        if(IsOutOfAmmo())
        {
            GameOver();
        }
    }

    /// <summary>
    /// Activates the gameover screen
    /// </summary>
    private void GameOver()
    {
        _gameOverObject.SetActive(true);
    }
}
