﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon
{
    private int _maxAmmunition;
    public int _currentAmmunition;
    private int _explosionSize;

    /// <summary>
    /// Default constructor
    /// Sets variables
    /// </summary>
    /// <param name="maxAmmo">starting ammunition</param>
    /// <param name="explosionSize">affected tiles</param>
    public Weapon(int maxAmmo, int explosionSize = 1)
    {
        _maxAmmunition = maxAmmo;
        _currentAmmunition = _maxAmmunition;
        _explosionSize = explosionSize;
    }

    /// <summary>
    /// Called whenever the player shoots
    /// Returns affected tiles
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public RaycastHit2D[] Shoot(GameObject target = null)
    {
        _currentAmmunition--;

        if (target != null)
        {
            Vector2 origin = new Vector2(target.transform.position.x, target.transform.position.y);
            Vector2 size = new Vector2(_explosionSize, _explosionSize);
            RaycastHit2D[] hits = Physics2D.BoxCastAll(origin, size, 0, new Vector2(0, 0));
            return hits;
        }
        
    return null;
    }
    
}
